import { useEffect, useState } from "react";

import { SearchInput } from "../../components/search-input/search-input";
import { Loader } from "../../components/loader/loader";
import { ProjectsList } from "../../components/projects-list/project-list";
import { ErrorComponent } from "../../components/error/error";

import { sortByDate } from "../../utils/sort-dates";
import { githubUsernameRegex } from "../../utils/constants";

import "./search-page.scss";
import { useAppDispatch, useAppSelector } from "../../utils/hooks";
import {
  clearErrorAction,
  getProjectDataAction,
  getUserDataAction,
  selectProjectsDetailsResults,
  selectSearchError,
  selectSearchIsLoading,
  selectUserDetailsResults,
  setErrorAction,
} from "../../store/slices/search";

export const SearchPage = () => {
  const [username, setUsername] = useState("");

  const isLoading = useAppSelector(selectSearchIsLoading);
  const error = useAppSelector(selectSearchError);
  const userData = useAppSelector(selectUserDetailsResults);
  const projects = useAppSelector(selectProjectsDetailsResults);

  const dispatch = useAppDispatch();

  const clearError = () => dispatch(clearErrorAction());

  useEffect(() => {
    if (userData) {
      const sortedProjects = sortByDate(userData, "updated_at");
      sortedProjects.forEach((project: any, idx) => {
        dispatch(getProjectDataAction(project));
      });
    }
  }, [userData]);

  const handleSearch = () => {
    const isValid = validateUsername(username);
    if (isValid) {
      dispatch(getUserDataAction(username));
    } else {
      dispatch(
        setErrorAction({
          errorMessage: "Podaj prawidłowy login użytkownika",
        })
      );
    }
  };

  const validateUsername = (name: string) => githubUsernameRegex.test(name);

  return (
    <main>
      <SearchInput
        handleSearch={handleSearch}
        setValue={setUsername}
        setError={clearError}
        error={error}
        value={username}
        placeholder={"Wpisz login użytkownika GitHub"}
        ctaText={"Szukaj"}
        disabled={isLoading}
      />
      {isLoading && <Loader />}
      {!isLoading && error ? (
        <ErrorComponent message={error} />
      ) : (
        projects?.length > 0 && <ProjectsList projects={projects} />
      )}
    </main>
  );
};
