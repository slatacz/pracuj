import { useEffect, useState } from "react";
import Cookies from "js-cookie";

import { Loader } from "../../components/loader/loader";
import { SearchInput } from "../../components/search-input/search-input";
import { ErrorComponent } from "../../components/error/error";
import { CalculationHistory } from "../../components/calculation-history/calculation-history";

import { integerRegex } from "../../utils/constants";
import { IHistoryValue } from "../../types/calculation-types";

import "./strong-page.scss";

export const StrongPage = () => {
  const [isLoading, setLoading] = useState(false);
  const [value, setValue] = useState("");
  const [error, setError] = useState<string | null>(null);
  const [history, setHistory] = useState<IHistoryValue[] | null>(null);

  const validateValue = (value: string) => integerRegex.test(value);

  const calcFactorial = (n: number): number => {
    if (n === 0 || n === 1) {
      return 1;
    } else {
      return n * calcFactorial(n - 1);
    }
  };

  const saveHistoryToCookie = (history: IHistoryValue[] | null) => {
    if (history) {
      Cookies.set("factorialHistory", JSON.stringify(history));
    }
  };

  const getHistoryFromCookie = () => {
    const cookieData = Cookies.get("factorialHistory");
    if (cookieData) {
      return JSON.parse(cookieData);
    }

    return null;
  };

  const handleSubmit = () => {
    setLoading(true);
    const isValid = validateValue(value);
    if (isValid) {
      const numericValue = parseInt(value);
      const factorial = calcFactorial(numericValue);
      const historyElement = {
        base: numericValue,
        factorial,
      };
      setHistory((prev) =>
        !prev ? [historyElement] : [...prev, historyElement]
      );
      setLoading(false);
    } else {
      setError("Liczba musi być całkowita");
      setLoading(false);
    }
  };

  useEffect(() => {
    if (!history) {
      setLoading(true);
      const calculationsHistory = getHistoryFromCookie();
      setHistory(calculationsHistory);
      setLoading(false);
    }

    return saveHistoryToCookie(history);
  }, []);

  return (
    <main>
      <SearchInput
        handleSearch={handleSubmit}
        setValue={setValue}
        setError={setError}
        error={error}
        value={value}
        disabled={isLoading}
        placeholder={"Wpisz liczbę"}
        ctaText={"Oblicz"}
      />
      {isLoading && <Loader />}
      {/* TODO Create result & history components */}
      {!isLoading && error ? (
        <ErrorComponent message={error} />
      ) : (
        <CalculationHistory values={history} />
      )}
    </main>
  );
};
