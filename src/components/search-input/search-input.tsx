import { useState } from "react";
import "./search-input.scss";
import { handleClassnames } from "../../utils/handle-classnames";

interface ISearchInputProps {
  value: string | number;
  placeholder: string;
  ctaText: string;
  disabled: boolean;
  setValue: (newValue: string) => void;
  handleSearch: () => void;
  setError: (error: string | null) => void;
  error: string | null;
}

export const SearchInput = ({
  value,
  disabled,
  placeholder,
  ctaText,
  error,
  setValue,
  handleSearch,
  setError,
}: ISearchInputProps) => {
  const [isActive, setActive] = useState(false);

  const handleFocus = () => {
    setError(null);
    setActive(true);
  };

  return (
    <div
      className={handleClassnames("inputContainer", {
        active: isActive,
        disabled: disabled,
        error: !!error,
      })}
    >
      <input
        type={"text"}
        placeholder={placeholder}
        value={value}
        disabled={disabled}
        onFocus={handleFocus}
        onBlur={() => setActive(false)}
        onChange={(e) => setValue(e.target.value)}
      />
      <button onClick={handleSearch}>{ctaText}</button>
    </div>
  );
};
