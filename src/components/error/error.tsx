import "./error.scss";

export const ErrorComponent = ({ message }: { message: string }) => (
  <div className="error-container">
    <p>{message}</p>
  </div>
);
