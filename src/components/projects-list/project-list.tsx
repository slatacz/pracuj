import { Link } from "react-router-dom";
import "./project-list.scss";

export const ProjectsList = ({ projects }: { projects: any[] }) => (
  <section className="project-list">
    {projects.map((project) => (
      <SingleProject key={project.node_id} project={project} />
    ))}
  </section>
);

const SingleProject = ({ project }: any) => (
  <article className="project-list__card">
    <p>{project.name}</p>

    <ul className="project-list__card__commit-list">
      {project.commits?.map((commit: any) => {
        const { sha, html_url: url } = commit;

        return (
          sha && (
            <li key={sha}>
              <Link to={url} target="_blank">
                {sha}
              </Link>
            </li>
          )
        );
      })}
    </ul>
  </article>
);
