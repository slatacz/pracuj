import { IHistoryValue } from "../../types/calculation-types";
import "./calculation-history.scss";

export const CalculationHistory = ({
  values,
}: {
  values: IHistoryValue[] | null;
}) =>
  values ? (
    <div className="calculations-container">
      <ul>
        {values.map((value) => (
          <li>
            <p>
              Liczba: {value.base} silnia: {value.factorial}
            </p>
          </li>
        ))}
      </ul>
    </div>
  ) : null;
