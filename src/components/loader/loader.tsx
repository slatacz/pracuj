import "./loader.scss";

export const Loader = () => (
  <div className="loader-container">
    <div className="loader-container__dot" />
    <div className="loader-container__dot" />
    <div className="loader-container__dot" />
    <div className="loader-container__dot" />
    <div className="loader-container__dot" />
  </div>
);
