import { NavLink } from "react-router-dom";

import "./header.scss";

export const Header = () => (
  <header className="mainNav">
    <NavLink to="/">Wyszukaj projekty</NavLink>
    <NavLink to="zalozenia-projektu">Założenia</NavLink>
    <NavLink to="kalkulator">Oblicz silnię</NavLink>
  </header>
);
