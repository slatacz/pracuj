import { API_URL } from "../utils/constants";

export const getUserDataRequest = async (username: string) => {
  const response = await fetch(
    `${API_URL}/users/${username}/repos?sort=updated&per_page=5`
  ).then((res) => res.json());

  return response;
};

export const getProjectDataRequest = async (project: any) => {
  const response = await fetch(
    `${API_URL}/repos/${project.full_name}/commits?per_page=5`
  ).then((res) => res.json());

  return {
    ...project,
    commits: Array.isArray(response) ? response : [response],
  };
};
