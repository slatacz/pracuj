export type IHistoryValue = {
  base: number;
  factorial: number;
};
