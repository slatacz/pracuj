import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { getProjectDataRequest, getUserDataRequest } from "../../api/reqests";
import { DEFAULT_ERROR_MSG } from "../../utils/constants";
import { RootState } from "../store";

export type ISearchState = {
  userDetails: any[];
  projectsDetails: any[];
  isLoading: boolean;
  error: string | null;
};

const initialState: ISearchState = {
  userDetails: [],
  projectsDetails: [],
  isLoading: false,
  error: null,
};

export const getUserDataAction = createAsyncThunk(
  "search/getUserDataAction",
  getUserDataRequest
);

export const getProjectDataAction = createAsyncThunk(
  "search/getProjectDataAction",
  getProjectDataRequest
);

export const searchSlice = createSlice({
  name: "search",
  initialState,
  reducers: {
    clearErrorAction: (state) => {
      state.error = null;
    },
    setErrorAction: (
      state,
      { payload: { errorMessage } }: { payload: { errorMessage: string } }
    ) => {
      state.error = errorMessage;
      state.isLoading = false;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getUserDataAction.pending, (state) => {
        state.isLoading = true;
        state.projectsDetails = [];
      })
      .addCase(getUserDataAction.fulfilled, (state, action) => {
        if (action.payload.message) {
          state.error = action.payload.message;
        } else {
          state.userDetails = action.payload;
        }
        state.isLoading = false;
      })
      .addCase(getUserDataAction.rejected, (state, action) => {
        state.isLoading = false;
        state.error = action.error.message ?? DEFAULT_ERROR_MSG;
      })

      .addCase(getProjectDataAction.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(getProjectDataAction.fulfilled, (state, action) => {
        state.isLoading = false;

        if (state.projectsDetails) {
          state.projectsDetails = [...state.projectsDetails, action.payload];
        } else {
          state.projectsDetails = [action.payload];
        }
      })
      .addCase(getProjectDataAction.rejected, (state, action) => {
        state.isLoading = false;
        state.error = action.error.message ?? DEFAULT_ERROR_MSG;
      });
  },
});

export const { clearErrorAction, setErrorAction } = searchSlice.actions;

export const selectSearchIsLoading = (state: RootState) =>
  state.search.isLoading;
export const selectUserDetailsResults = (state: RootState) =>
  state.search.userDetails;
export const selectProjectsDetailsResults = (state: RootState) =>
  state.search.projectsDetails;
export const selectSearchError = (state: RootState) => state.search.error;

export const searchReducer = searchSlice.reducer;
