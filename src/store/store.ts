import {
  Action,
  AnyAction,
  combineReducers,
  configureStore,
  ThunkAction,
} from "@reduxjs/toolkit";

import { searchReducer } from "./slices/search";

export type Store = ReturnType<typeof makeStore>;
export type AppDispatch = Store["dispatch"];
export type RootState = ReturnType<Store["getState"]>;
export type StateType = ReturnType<typeof combinedReducer>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;

const combinedReducer = combineReducers({
  search: searchReducer,
});

const mainReducer = (state: StateType | undefined, action: AnyAction) => {
  return combinedReducer(state, action);
};

export const makeStore = () =>
  configureStore({
    reducer: mainReducer,
  });
