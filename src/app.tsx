import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { Provider } from "react-redux";
import { makeStore } from "./store/store";

import { SearchPage } from "./pages/search-page/search-page";
import { RequirementsPage } from "./pages/requirements-page/requirements-page";

import { Header } from "./components/header/header";
import { Footer } from "./components/footer/footer";
import { StrongPage } from "./pages/strong-page/strong-page";

function App() {
  const store = makeStore();

  return (
    <Provider store={store}>
      <Router>
        <Header />
        <Routes>
          <Route path="/" element={<SearchPage />} />
          <Route path="/zalozenia-projektu" element={<RequirementsPage />} />
          <Route path="/kalkulator" element={<StrongPage />} />
        </Routes>
        <Footer />
      </Router>
    </Provider>
  );
}

export default App;
