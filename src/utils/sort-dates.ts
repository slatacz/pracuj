export const sortByDate = <T>(objects: T[], dateName: keyof T) => {
  const objectsWithDateProps = objects.map((obj) => ({
    ...obj,
    dateObject: new Date(obj[dateName] as string),
  }));

  objectsWithDateProps.sort(
    (a, b) =>
      (a.dateObject as Date).getTime() - (b.dateObject as Date).getTime()
  );

  const sortedObjects = objectsWithDateProps.map(
    ({ dateObject, ...rest }) => rest
  );

  return sortedObjects;
};
