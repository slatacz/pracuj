export const githubUsernameRegex = /^[a-z\d](?:[a-z\d]|-(?=[a-z\d])){0,38}$/i;
export const integerRegex = /^-?\d+$/i;

export const DEFAULT_ERROR_MSG = "Podaj prawidłowy logit użytkownika";

export const API_URL = "https://api.github.com";
