export const handleClassnames = (
  baseClassName: string,
  modifiers?: {
    [key: string]: boolean;
  }
): string => {
  const classNames = [baseClassName];

  for (const modifier in modifiers) {
    if (modifiers[modifier]) {
      classNames.push(`${baseClassName}--${modifier}`);
    }
  }

  return classNames.join(" ");
};
